FROM ubuntu:14.04

# Install the build-essential package
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
    && apt-get -y install apt-utils --assume-yes \
    && apt-get -y install wget tar curl build-essential git jq netcat libpng12-dev zip \
    && rm -rf /var/lib/apt/lists/*

# Download and install Go 1.8 for Linux, Set up your Go workspace:
RUN wget https://storage.googleapis.com/golang/go1.8.linux-amd64.tar.gz \
    && tar -C /usr/local -xzf go1.8.linux-amd64.tar.gz \
    && mkdir /go

ENV GOPATH=/go PATH=$PATH:/go/bin:/usr/local/go/bin:/mattermost/bin

# Install Node.js and Yarn
RUN curl -sL https://deb.nodesource.com/setup_7.x | bash - \
    && apt-get install -y nodejs \
    && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update && apt-get install -yq yarn

# Copy the code from your forked repository
RUN mkdir -p /go/src/github.com/mattermost/platform
COPY . /go/src/github.com/mattermost/platform

# Ready to compile
RUN cd /go/src/github.com/mattermost/platform \
    && make package-linux \
    && cp -rf /go/src/github.com/mattermost/platform/dist/mattermost /mattermost \
    && rm -rf /go \
    && mkdir -p /mattermost/data

EXPOSE 80

VOLUME /mattermost/data

WORKDIR /mattermost/bin
CMD ["platform"]